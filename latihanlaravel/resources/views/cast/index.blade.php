@extends('layout.master')
@section('judul')
Halaman List Cast
@endsection
@section('content')

<a href="/cast/create" class="btn btn-primary btn-sm my-2">Add new Cast</a>

<table class="table">
    <thead class="thead-dark">
      <tr>
        <th scope="col">#</th>
        <th scope="col">Name</th>
        <th scope="col">Age</th>
        <th scope="col">Bio</th>
        <th scope="col">Action</th>
      </tr>
    </thead>
    <tbody>
        @forelse ($cast as $key=>$item)
            <tr>
                <td>{{$key+1}}</td>
                <td>{{$item->Name}}</td>
                <td>{{$item->Age}}</td>
                <td style="max-width: 500px;">{{$item->Bio}}</td>
                <td>
                    <form action="/cast/{{$item->id}}" method="POST">
                    @csrf
                    @method('delete')
                    <a href="/cast/{{$item->id}}" class="btn btn-sm btn-info">Detail</a>
                    <a href="/cast/{{$item->id}}/edit" class="btn btn-sm btn-warning">Edit</a>
                    <input type="submit" class="btn btn-sm btn-danger" value="Delete">

                </form>
                </td>
            </tr>
        @empty
            <tr>
                <td>
                    Data is Empty
                </td>
            </tr>
            
        @endforelse
    </tbody>
  </table>
  

@endsection