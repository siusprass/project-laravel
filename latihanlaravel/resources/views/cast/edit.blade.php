@extends('layout.master')
@section('judul')
Halaman Edit Cast
@endsection
@section('content')
<h2>New Cast</h2>
<form action="/cast/{{$cast->id}}" method="POST">
    @csrf
    @method('put')
        <div class="form-group">
          <label>Cast name</label>
          <input type="text" value="{{$cast->Name}}" name="Name" class="form-control">
        </div>
    @error('Name')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror
        <div class="form-group">
          <label>Age</label><br>
          <input type="text" value="{{$cast->Age}}" name="Age" class="form-control" placeholder="Enter cast age here">
        </div>
    @error('Age')
        <div class="alert alert-danger">{{ $message }}</div>
    @enderror
        <div class="form-group">
            <label>Bio</label><br>
            <textarea name="Bio" class="form-control" cols="30" rows="10" placeholder="Enter cast biography here">{{$cast->Bio}}</textarea>
          </div>
    @error('Bio')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror
        <button type="submit" class="btn btn-primary">Submit</button>
</form>
@endsection