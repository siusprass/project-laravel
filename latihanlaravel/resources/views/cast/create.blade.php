@extends('layout.master')
@section('judul')
Halaman Create Cast
@endsection
@section('content')
<h2>New Cast</h2>
<form action="/cast" method="POST">
    @csrf
        <div class="form-group">
          <label>Cast name</label>
          <input type="text" name="Name" class="form-control">
        </div>
    @error('Name')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror
        <div class="form-group">
          <label>Age</label><br>
          <input type="text" name="Age" class="form-control" placeholder="Enter cast age here">
        </div>
    @error('Age')
        <div class="alert alert-danger">{{ $message }}</div>
    @enderror
        <div class="form-group">
            <label>Bio</label><br>
            <textarea name="Bio" class="form-control" cols="30" rows="10" placeholder="Enter cast biography here"></textarea>
          </div>
    @error('Bio')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror
        <button type="submit" class="btn btn-primary">Submit</button>
</form>
@endsection