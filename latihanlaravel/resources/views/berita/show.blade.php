@extends('layout.master')
@section('judul')
Halaman list berita
@endsection
@section('content')
<a href="/berita/create" class="btn btn-primary">tambah berita</a><br><br>
<div class="row">
    <div class="col-12">
        <div class="card" style="width: 100%">
            <img class="card-img-top" src={{asset('images/'.$berita->thumbnail)}} alt="Card image cap">
            <div class="card-body">
                <h2>judul</h2>
                <h2 class="card-title">{{$berita->judul}}</h2><br>
                <h3>isi berita</h3>
                <p class="card-text">{{$berita->content}}</p>
                <h5>penulis : {{$berita->penulis}} </h5>
                <h6 clas="card-text">kategori :{{$berita->kategori_id}}</h6>
                <a href="/berita" class="btn btn-primary">kembali</a>
            </div>
          </div>

    </div>
</div>

@endsection