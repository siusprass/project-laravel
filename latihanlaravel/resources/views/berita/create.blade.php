@extends('layout.master')
@section('judul')
Halaman Create berita
@endsection
@section('content')
<h2>New berita</h2>
<form action="/berita" method="POST" enctype="multipart/form-data">
    @csrf
        <div class="form-group">
          <label>judul</label>
          <input type="text" name="judul" class="form-control">
        </div>

        <div class="form-group">
            <label>Content</label><br>
            <textarea name="content" class="form-control" cols="30" rows="10" placeholder="Enter cast biography here"></textarea>
          </div>

          <div class="form-group">
            <label>thumbnails</label>
            <input type="file" name="thumbnail" class="form-control">
          </div>
          <div class="form-group">
            <label>penulis</label>
            <input type="text" name="penulis" class="form-control">
          </div>
          <div class="form-group">
            <label>kategori id</label>
            <input type="text" name="kategori_id" class="form-control">
          </div>
          
          {{-- <div class="form-group">
            <label>kategori id</label>
            <select name="kategori_id" class="form-control">
            <option value="">--select kategori--</option>
                @foreach ($kategori as $item)
                    <option value="{{$item->id}}">{{$item=Name}}</option>
                @endforeach
            </select>
          </div> --}}

        <button type="submit" class="btn btn-primary">Submit</button>
</form>
@endsection