@extends('layout.master')
@section('judul')
Halaman list berita
@endsection
@section('content')

<div class="row">
    @forelse ($berita as $item)
    <div class="col-12">
        <div class="card" style="">
            <img class="card-img-top" style="max-width: 400px; margin:2%" src={{asset('images/'.$item->thumbnail)}} alt="Card image cap">
            <div class="card-body">
              <h5 class="card-title">{{$item->judul}}</h5>
              <p class="card-text">{{Str::limit($item->content),20}}</p>
              <a href="/berita/{{$item->id}}" class="btn btn-primary">Detail topic</a>
            </div>
          </div>

    </div>
    @empty
    <h1>Tidak ada list yang ditampilkan</h1>
        
    @endforelse
    
</div>

@endsection