@extends('layout.master')
@section('judul')
Halaman pendaftaran
@endsection
@section('content')
<h1>Buat Account Baru!</h1>
<h2>Sign Up Form</h2>
<form action="/doneReg">
	<label>First name :</label> <br>
	<input type="text" id="Fname"> <br>
	<label>Last Name :</label> <br>
	<input type="text" id="Lname"> <br>
	<br>
		Gender : <br>
		<input type="radio" name="gender" value="Male"> <label>Male</label> <br>
		<input type="radio" name="gender" value="Female"> <label>Female</label> <br>
		<input type="radio" name="gender" value="Other"> <label>Other</label> 
		<br>
		<br>
		<label>Nationality :</label> <br>
		<select name="nationality"> 
			<option value="IDN">Indonesia</option>
			<option value="SEA">Singapore</option>
			<option value="MLY">Malaysia</option>
			<option value="Oceania">Australia</option>
		</select>
		<br>
		<br>
		languange spoken :
		<br>
		<input type="checkbox" name="Lang" value="Id"> <label>Bahasa Indonesia</label> <br>
		<input type="checkbox" name="Lang" value="English"> <label>English</label> <br>
		<input type="checkbox" name="Lang" value="Other"> <label>Other</label> <br>
		<br>
		Bio :
		<br>
		<textarea name="Bio" value="bio" cols="30" rows="10">
		</textarea>
		<br>
		<br>
		<input type="submit" name="">
	</form>
@endsection