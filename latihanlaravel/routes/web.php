<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'indexController@dashboard');
Route::get('/Form', 'register@Reg');
Route::get('/doneReg','welcome@register');
Route::get('/data-table',function(){
    return view('pages.data-table');
});
Route::get('/table',function(){
    return view('pages.table');
});

//CRUD Cast

//Create

//form input data create cast
Route::get('/cast/create', 'CastController@create');
//for saving into database
Route::post('/cast', 'CastController@store');


//Read

//showing all inputted data in the table
Route::get('/cast', 'CastController@index');

//showing detail in cast
Route::get('/cast/{cast_id}','CastController@show');

//Update
//update Cast data with updated version
Route::get('/cast/{cast_id}/edit','CastController@edit');
//Update data base on id cast
Route::put('/cast/{cast_id}', 'CastController@update');

//delete


Route::delete('/cast/{cast_id}', 'CastController@destroy');
//crud berita
Route::resource('berita', 'BeritaController');
?>