<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class CastController extends Controller
{
    //
    public function create(){
        return view('cast.create');
    }

    public function store(Request $request){
        // dd($request->all());
        $request->validate([
            'Name' => 'required',
            'Age' => 'required',
            'Bio' => 'required',
        ],
        [
            'Name.required' => 'Cast name Required!',
            'Age.required' => 'Cast Age Required!',
            'Bio.required' => 'Cast Bio Required!',
        ]);

        
        return redirect('/cast');

    }

    public function index(){
        $cast = DB::table('cast')->get();
        // dd($cast);
 
        return view('cast.index', compact('cast'));

    }

    public function show($id){
        $cast = DB::table('cast')->where('id', $id)->first();

        return view('cast.show',compact('cast'));
    }

    public function edit($id){
        $cast = DB::table('cast')->where('id', $id)->first();

        return view('cast.edit',compact('cast'));
    }

    public function update($id,Request $request){
        $request->validate([
            'Name' => 'required',
            'Age' => 'required',
            'Bio' => 'required',
        ],
        [
            'Name.required' => 'Cast name Required!',
            'Age.required' => 'Cast Age Required!',
            'Bio.required' => 'Cast Bio Required!',
        ]);

        $affected = DB::table('cast')
              ->where('id', $id)
              ->update(
                  [
                  'Name' => $request['Name'],
                  'Age' => $request['Age'],
                  'Bio' => $request['Bio'],
                  ]
            );
        return redirect ('/cast');
    }
    
    public function destroy($id){
        DB::table('cast')->where('id', '=', $id)->delete();
        return redirect('/cast');
    }
}
