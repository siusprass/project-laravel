<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class kategori extends Model
{
    //
    protected $table = 'kategori';

    protected $fillable = ['judul','content','thumbnail','penulis','kategori_id'];
}
